FROM composer:latest

# Install laravel/installer
RUN composer global require laravel/installer

# Add vendor/bin to PATH
ENV PATH="${COMPOSER_HOME:-/tmp}/vendor/bin:${PATH}"

# Set entrypoint
ENTRYPOINT [ "/docker-entrypoint.sh", "laravel" ]
